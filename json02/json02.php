<?php
/*Este proyecto se genero con informacion del video
Curso de PHP 57 - Crear y leer archivos con formato json
url: https://www.youtube.com/watch?v=zFlDkbzUifo
2019-10-01
*/
require "php-json-file-decode-master/json-file-decode.class.php";
//creamos una estructura JSON a travez de un array
$alumnos= array(
    "alumnos"=>array(
        array("nombre"=>"Daniel", "apellido"=>"aguilar", "calificacion"=>10),
        array("nombre"=>"Rosa", "apellido"=>"ramirez", "calificacion"=>9.5),
        array("nombre"=>"Rosalio", "apellido"=>"pina", "calificacion"=>9)
    )
);
//convertimos el array a formato JSON
$joson_alumnos = json_encode($alumnos);
//se utiiliza var_dup para ver los datos en pantalla
var_dump($joson_alumnos); 
//Para guardar en un archivo JSON
$handler = fopen("json02.json", "w+");
//excribimos los archivos de la variable $joson_alumnos en el archivo 
fwrite($handler,$joson_alumnos);
//
fclose($handler);
echo "<br>";
echo "archivo json02 se ha creado con exito";
echo "<br>";
//accediendo a los archivos del json
/*para ello es necesario requerir el archivo json-file-decode.class.php
* incluido en la carpeta descargada php-json-file-decode-master
*este archivo se descargo del repositorio de git
https://github.com/ManuDavila/php-json-file-decode.git
*/
//estas instruccione vienen incluida en el archivo readme de la carpeta descargada de git 
$read = new json_file_decode();
//incluimos el archivo que vamos a leer
//json02.json
$json = $read->json("json02.json");
echo "Salida del archivo json02.json";
echo "<br>";
print_r($json);
//acceder al nombre del primer alumno
echo "<br>";
echo "El nombre del primer alumno es: " .
//$json = variable que almacena los datos
//["alumnos"] es el archivo json que leimos con la clase importada
//[0] =  a la posicion dentro del arreglo json
//["nombre"] = campo al que llamamos 
$json["alumnos"][0]["nombre"] . 
//mostramos la calificacion
" y su calificacion es: " .
$json["alumnos"][0]["calificacion"];
?>